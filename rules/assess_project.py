def rule_project_dir_must_be_dir(project_dir):
    assert project_dir.is_dir()


def rule_project_dir_must_be_git_working_dir(project_dir):
    dotgit = project_dir / ".git"
    assert dotgit.is_dir()


def rule_project_must_have_toplevel_editorconfig(project_dir):
    editorconfig = project_dir / ".editorconfig"
    assert editorconfig.is_file()
