import pytest
from pathlib import Path


def pytest_addoption(parser):
    parser.addoption("--project-dir", type=Path)


@pytest.fixture
def project_dir(request):
    return request.config.getoption("--project-dir")
