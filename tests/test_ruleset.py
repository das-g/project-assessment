def test_nonexistant_path_fails_all_rules(testdir, rules_path):

    projdir_args = "--project-dir", "does-not-exist"
    result = testdir.runpytest(rules_path, *projdir_args)

    result.assert_outcomes(failed=3)


def test_own_repo_passes_two_rules_and_fails_one(testdir, rules_path, own_project_path):
    """
    It's a dir and a Git repo, but doesn't contain an .editorconfig file.
    """

    projdir_args = "--project-dir", str(own_project_path)
    result = testdir.runpytest(rules_path, *projdir_args)

    result.assert_outcomes(passed=2, failed=1)
