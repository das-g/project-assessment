import pytest
from pathlib import Path

pytest_plugins = "pytester"


@pytest.fixture
def rules_path(own_project_path):
    return str(own_project_path.resolve() / 'rules')


@pytest.fixture
def own_project_path():
    conftest_location = Path(__file__)
    tests_dir = conftest_location.parent
    project_root_dir = tests_dir.parent
    return project_root_dir
