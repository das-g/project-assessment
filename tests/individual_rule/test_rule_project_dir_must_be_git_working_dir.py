# Test checking of a single rule:
RULES_SELECTION = "-k", "rule_project_dir_must_be_git_working_dir"


def test_fails_for_nonexistant_path(testdir, rules_path):

    projdir_args = "--project-dir", "does-not-exist"
    result = testdir.runpytest(rules_path, *RULES_SELECTION, *projdir_args)

    result.assert_outcomes(failed=1)


def test_fails_for_regular_file(testdir, rules_path, tmpdir):
    file_path = tmpdir / "some-file.txt"
    file_path.write("dummy content")

    projdir_args = "--project-dir", file_path
    result = testdir.runpytest(rules_path, *RULES_SELECTION, *projdir_args)

    result.assert_outcomes(failed=1)


def test_fails_for_empty_dir(testdir, rules_path, tmpdir):
    dir_path = tmpdir / "some-dir"
    dir_path.mkdir()

    projdir_args = "--project-dir", dir_path
    result = testdir.runpytest(rules_path, *RULES_SELECTION, *projdir_args)

    result.assert_outcomes(failed=1)


def test_fails_for_nonempty_nongit_dir(testdir, rules_path, tmpdir):
    file_path = tmpdir / "some-file.txt"
    file_path.write("dummy content")

    projdir_args = "--project-dir", tmpdir
    result = testdir.runpytest(rules_path, *RULES_SELECTION, *projdir_args)

    result.assert_outcomes(failed=1)


def test_passes_for_dir_with_dotgit_subdir(testdir, rules_path, tmpdir):
    dotgit = tmpdir / ".git"
    dotgit.mkdir()

    projdir_args = "--project-dir", tmpdir
    result = testdir.runpytest(rules_path, *RULES_SELECTION, *projdir_args)

    result.assert_outcomes(passed=1)
